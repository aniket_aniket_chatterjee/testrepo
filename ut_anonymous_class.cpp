#define BOOST_TEST_MODULE mytest
#include "ut_anonymous_class.h"
#include "anonymousclass.h"
#include "js_engine.h"


using namespace boost::unit_test;

UT_AnonymousClass::UT_AnonymousClass():test_suite("Anonymous_Unit")
{
	
}

bool UT_AnonymousClass:: testGetInstance(std::string str)
{
	AnonymousClass *ac=	AnonymousClass::GetInstance(str);
	BOOST_CHECK(ac!=NULL);
	if(ac!=NULL)
			return true;
		else
			return false;
			
}

bool UT_AnonymousClass::testcChkInstance(std::string str)
{
		AnonymousClass* ac=	AnonymousClass::GetInstance(str);
		AnonymousClass* ac1=AnonymousClass::ChkInstance(str.c_str());
		if(ac1==ac1)
			return true;
		else
			return false;
}

bool UT_AnonymousClass::testRegister(JSEngine &_js)
{
		AnonymousClass *ac=	AnonymousClass::GetInstance("MyTest");
		
		int i=_js.Init();
		ac->Register(_js);
		return true;
}
bool UT_AnonymousClass::testGetJSClass(std::string str)
{
		JSClass _jsClass;
		AnonymousClass *ac=	AnonymousClass::GetInstance(str);
		try{
				_jsClass=ac->getJSClass();
		}
		catch(std::exception ex)
		{
				return false;
				
		}
		return true;
		
}