#include "ut_anonymousjs_object.h"
#include "js_console.h"
#include <fstream>
char* TEST_FILE="test.js";

UT_AnonymousJSObject::UT_AnonymousJSObject()
{
}

UT_AnonymousJSObject::~UT_AnonymousJSObject()
{
}
void UT_AnonymousJSObject::genScript(std::string str)
{
		std::ofstream outfile;
		outfile.open("test.js");
		outfile<<str.c_str();
		outfile.close();
}
//GetJSObject Test case
bool UT_AnonymousJSObject::testGetJSObject()
{
	AnonymousJSObject *ajo;
	JSEngine _js;
	int iret =  _js.Init();
	if(iret!=0)
	{
		printf("init failed\n");
		return false;
	}
	try{
	defConsole(_js);
	
	}
	catch(std::exception ex)
	{
		return false;
		}
	try{	
	 ajo= new AnonymousJSObject("MYOBJ",_js);	
	}
	catch(std::exception ex)
	{
		return false;
		}

	try{
			JSObject *jo=ajo->getJSObject();	
	}
	catch(std::exception ex)
	{
		return false;
		}
		return true;
}

bool UT_AnonymousJSObject::testAnonymousJSObject()
{
	JSEngine _js;
	int iret =  _js.Init();
	if(iret!=0)
	{
		printf("init failed\n");
		return false;
	}
	try{
	defConsole(_js);
	
	}
	catch(std::exception ex)
	{
		return false;
		}
	try{	
		AnonymousJSObject *ajo = new AnonymousJSObject("MYOBJ",_js);	
	}
	catch(std::exception ex)
	{
		return false;
		}
		return true;
}

void atestfunc()
{
	std::cout << "testfunc\n";
}

void testFuncParameter(std::string str)
{
	std::cout << "TestString\n"<<"sent string is - "<<str <<"\n";
}
char testFuncParameter2(std::string str)
{
	std::ostringstream ob;
	ob<<"Hello, "<<str;
	return ('x'); 
}


//testAddFunc Test case without parameter
bool UT_AnonymousJSObject::testAddFunc()
{
				AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
				ajo->addFunc<atestfunc>("testfunc", atestfunc);
				}
				catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="MYOBJ.testfunc();\n";
				genScript(str);
				
				try {
					
						int res = _js.CompileExecuteFile(TEST_FILE);
					}
					catch(std::exception ex)
				{
					return false;
				}
			return true;						
	} 

//testAddFunc Test case with parameter
bool UT_AnonymousJSObject::testAddFuncWithParam()
{
		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				ajo=new AnonymousJSObject("MYOBJ",_js);
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
				ajo->addFunc<void, std::string, testFuncParameter>("testString", testFuncParameter);
				}
				catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="MYOBJ.testString(\"Hello World\");";
				genScript(str);
				try {
						int res = _js.CompileExecuteFile(TEST_FILE);
					}
					catch(std::exception ex)
				{
					return false;
				}
			return true;				
}

//testSetPropertyValdouble Test case for double false false
bool UT_AnonymousJSObject::testSetPropertyValdoubleff()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
				}
				try{	
					ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
				}
					
				if(!ajo->SetPropertyVal((char*)"Var8",12.13,false,false)){
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var8=\"+MYOBJ.Var8);\n\
								CONSOLE.writeln(\"Changing the Value of Var8 to 0.155\");\n\
								MYOBJ.Var8=0.155;\n\
								CONSOLE.writeln(\"Var8=\"+MYOBJ.Var8);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}

//testSetPropertyValdouble Test case for double true true
bool UT_AnonymousJSObject::testSetPropertyValdoublett()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",12.13,true,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 0.666\");\n\
								MYOBJ.Var2=0.666;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for double true false
bool UT_AnonymousJSObject::testSetPropertyValdoubletf()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",12.13,true,false);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 0.122\");\n\
								MYOBJ.Var2=0.122;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for double false true
bool UT_AnonymousJSObject::testSetPropertyValdoubleft()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",12.13,false,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 0.551\");\n\
								MYOBJ.Var2=0.551;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}

//testSetPropertyValdouble Test case for int false false
bool UT_AnonymousJSObject::testSetPropertyValintff()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",1107,false,false);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 555\");\n\
								MYOBJ.Var2=555;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}

//testSetPropertyValdouble Test case for int true true
bool UT_AnonymousJSObject::testSetPropertyValinttt()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",1107,true,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 7221\");\n\
								MYOBJ.Var2=7221;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for int false true
bool UT_AnonymousJSObject::testSetPropertyValintft()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",1107,false,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 7011\");\n\
								MYOBJ.Var2=7011;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for int true false
bool UT_AnonymousJSObject::testSetPropertyValinttf()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2",1107,true,false);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 155\");\n\
								MYOBJ.Var2=155;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
//testSetPropertyValdouble Test case for Regular String 
bool UT_AnonymousJSObject::testSetPropertyValstringRegular()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2","HelloWorld",false,false);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to World Hello\");\n\
								MYOBJ.Var2=\"World Hello\";\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for String Using Read Only and Permanent
bool UT_AnonymousJSObject::testSetPropertyValstringReadOnlyPermanent()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2","Using ReadOnly and permanent",true,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to Hello Test World\");\n\
								MYOBJ.Var2=\"Hello Test World\";\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for String Using Permanent
bool UT_AnonymousJSObject::testSetPropertyValstringPermanent()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2","Using Permanent",false,true);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to Test World\");\n\
								MYOBJ.Var2=\"Test World\";\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	
//testSetPropertyValdouble Test case for String Using Read Only
bool UT_AnonymousJSObject::testSetPropertyValstringReadOnly()
  {
	  AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
						ajo->SetPropertyVal((char*)"Var2","Using ReadOnly",true,false);
				}
					catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to Hello ReadOnly\");\n\
								MYOBJ.Var2=\"Hello ReadOnly\";\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
				int res = _js.CompileExecuteFile(TEST_FILE);
		return true;
	}
	

//testGetPropertyVal Test case for String 
bool UT_AnonymousJSObject::testGetPropertyValString()
{
	
	std::string str="Hello";

		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
					ajo->SetPropertyVal((char*)"Var2","Universal",false,false);

				}	
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string strscpt="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to HelloUniversal\");\n\
								MYOBJ.Var2=\"HelloUniversal\";\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(strscpt);
				int res = _js.CompileExecuteFile(TEST_FILE);		
				ajo->getPropertyVal("Var2",str);

				//ajo->getPropertyVal((char*)"Var3",ax,s);--->>error no matching function to call 
				std::cout<<"\n\tVar2="<<str;
}


//testGetPropertyVal Test case for Double 
bool UT_AnonymousJSObject::testGetPropertyValDouble()
{
	
	std::string str="Hello";

					double dp=12.12;

		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
					ajo->SetPropertyVal((char*)"Var2",12.13,false,false);

				}	
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string strscpt="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 70.11\");\n\
								MYOBJ.Var2=70.11;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(strscpt);
				int res = _js.CompileExecuteFile(TEST_FILE);		
				ajo->getPropertyVal("Var2",dp);

				//ajo->getPropertyVal((char*)"Var3",ax,s);--->>error no matching function to call 
				std::cout<<"\n\tVar2="<<dp;
}


//testGetPropertyVal Test case for Int 
bool UT_AnonymousJSObject::testGetPropertyValInt()
{
	
	std::string str="Hello";
					int p=12,s=5;
		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{
					ajo->SetPropertyVal((char*)"Var2",1107,false,false);
				}	
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string strscpt="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n\
								CONSOLE.writeln(\"Changing the Value of Var2 to 7011\");\n\
								MYOBJ.Var2=7011;\n\
								CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(strscpt);
				int res = _js.CompileExecuteFile(TEST_FILE);		
				ajo->getPropertyVal("Var2",p);
				//ajo->getPropertyVal((char*)"Var3",ax,s);--->>error no matching function to call 
				std::cout<<"\n\tVar2="<<p;
}



//testDefPropertyValExternal Test case for Int

bool UT_AnonymousJSObject::testDefPropertyValExternalInt()
{

					int p=12,s=5;
		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{

				if(ajo->DefPropertyValExternalInt("Var2",p))
					std::cout<<"\n\nValue not set  with DefPropertyValExternalInt(\"Var2\",p))\n";
				if(!ajo->SetPropertyValExternalInt("Var2",123))
					std::cout<<"\n\nValue not set  with SetPropertyValExternalInt(\"Var2\",123))\n";
				
		}
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2)\n";
				genScript(str);
		try{		
		int res = _js.CompileExecuteFile(TEST_FILE);		
		
		}
		catch(std::exception ex)
		{
			return false;
		}

		ajo->getPropertyValExternalInt("Var2",p);
		
		std::cout<<"\n\tVar2="<<p;
}


//testDefPropertyValExternal Test case for String

bool UT_AnonymousJSObject::testDefPropertyValExternalString()
{
					std::string str="Hello";
					char* ax;
		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{

				if(!ajo->DefPropertyValExternalString("Var2",ax))//  ---> showing Memory access violation
					std::cout<<"\n\nValue not set  with DefPropertyValExternalString(\"Var2\",ax))\n";
				if(!ajo->SetPropertyValExternalString("Var2","WellCome"))//not working
					std::cout<<"\n\nValue not set  with SetPropertyValExternalString(\"Var2\",\"WelCome\"))\n";

				
		}
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string strscpt="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(strscpt);
		try{		
		int res = _js.CompileExecuteFile(TEST_FILE);		
		
		}
		catch(std::exception ex)
		{
			return false;
		}

		ajo->getPropertyValExternalString("Var2",str);
		std::cout<<"\n\tVar2="<<str;
}


//testDefPropertyValExternal Test case for Double 

bool UT_AnonymousJSObject::testDefPropertyValExternalDouble()
{
					double dp=12.12;

		AnonymousJSObject *ajo;
				JSEngine _js;
				int iret =  _js.Init();
				if(iret!=0)
				{
					printf("init failed\n");
					return false;
				}
				try{
				defConsole(_js);
				
				}
				catch(std::exception ex)
				{
					return false;
					}
				try{	
				 ajo= new AnonymousJSObject("MYOBJ",_js);	
				}
				catch(std::exception ex)
				{
					return false;
					}
					
				try{

				if(!ajo->DefPropertyValExternalDouble("Var2",dp))
					std::cout<<"\n\nValue not set  with DefPropertyValExternalDouble(\"Var2\",dp))\n";
				if(!ajo->SetPropertyValExternalDouble("Var2",1213.123))
					std::cout<<"\n\nValue not set  with SetPropertyValExternalDouble(\"Var2\",1213.123))\n";	
				
		}
			catch(std::exception ex)
				{
					return false;
				}
				//Generate Script in test.js
				std::string str="CONSOLE.writeln(\"Var2=\"+MYOBJ.Var2);\n";
				genScript(str);
		try{		
		int res = _js.CompileExecuteFile(TEST_FILE);		
		
		}
		catch(std::exception ex)
		{
			return false;
		}

		ajo->getPropertyValExternalDouble("Var2",dp);
		std::cout<<"\n\tVar2="<<dp;
}

