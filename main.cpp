#include "ut_anonymous_class.h"
#include "ut_anonymousjs_object.h"
#include <iostream>
#include <boost/test/test_tools.hpp>
#include <boost/test/included/unit_test.hpp>


//AnonymousClass UnitTest
BOOST_AUTO_TEST_CASE(Test1)
{
	UT_AnonymousClass utac;
	
	BOOST_WARN(utac.testcChkInstance("MyTest"));// std::cout<<"ChkInstance Test Successful..."<<std::endl;
	//else 
		//std::cout<<"ChkInstance Test Failed..."<<std::endl;

	BOOST_WARN(utac.testcChkInstance("MyTest\n"));	
	BOOST_WARN(utac.testcChkInstance("MyTest---@as"));
	BOOST_WARN(utac.testcChkInstance("_____"));
	BOOST_WARN(utac.testcChkInstance("%d,%f,%s,%182"));
	BOOST_WARN(utac.testcChkInstance("*"));	
	}
	
	BOOST_AUTO_TEST_CASE(Test2)
{
	UT_AnonymousClass utac;
	
	BOOST_WARN(utac.testGetInstance("Hope"));
	BOOST_WARN(utac.testGetInstance("MyTest\n"));	
	BOOST_WARN(utac.testGetInstance("MyTest---@as"));
	BOOST_WARN(utac.testGetInstance("_____"));
	BOOST_WARN(utac.testGetInstance("%d,%f,%s,%182"));
	BOOST_WARN(utac.testGetInstance("*"));
	BOOST_WARN(utac.testGetInstance("Hope"));
	}
	

	BOOST_AUTO_TEST_CASE(Test3)
	{
	UT_AnonymousClass utac;
	JSEngine _js;
	_js.Init();
	BOOST_WARN(utac.testRegister(_js));
	}
	
	BOOST_AUTO_TEST_CASE(Test4)
	{
		UT_AnonymousClass utac;
		BOOST_WARN(utac.testGetJSClass("HelloWorld"));
	}
	
	
//======================================================================================================


//AnonymousJSObject UnitTest
	
	//GetJSObject Test call
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest1)
	{
		std::cout<<"\nTest Case Number: 1\n";
		std::cout<<"GetJSObject Test Case Number: 1.1\n";
		std::cout<<"Name: GetJSObject Test\n";
		std::cout<<"Objective: Ensure weather the getJSObject() returning a JSObject Successfully or not\n";

		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testGetJSObject(),true);
		
		std::cout<<"\n\nOutcome: GetJSObject Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
	//AnonymousJSObject Test case
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest2)
	{
		std::cout<<"\nTest Case Number: 2\n";
		std::cout<<"AnonymousJSObject Test Case Number: 2.1\n";
		std::cout<<"Name: testAnonymousJSObject Test\n";
		std::cout<<"Objective: Ensure weather the testAnonymousJSObject() returning a AnonymousJSObject Successfully or not\n";
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testAnonymousJSObject(),true);
		
		std::cout<<"\n\nOutcome: AnonymousJSObject Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testAddFunc Test case without parameter
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest3_1)
	{
		std::cout<<"\nTest Case Number: 3\n";
		std::cout<<"testAddFunc Test Case Number: 3.1\n";
		std::cout<<"Name: testAddFunc Test\n";
		std::cout<<"Objective: Check weather testAddFunc() returns true when called without parameters\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testAddFunc(),true);
		
		std::cout<<"\n\nOutcome: testAddFunc() without parameter Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testAddFunc Test case with parameter	
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest3_2)
	{
		std::cout<<"\nTest Case Number: 3\n";
		std::cout<<"testAddFunc Test Case Number: 3.2\n";
		std::cout<<"Name: testAddFunc Test\n";
		std::cout<<"Objective: Ensure testAddFunc() is taking parameters\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testAddFuncWithParam(),true);
		
		std::cout<<"\n\nOutcome: testAddFunc() with parameter Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for String Regular
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_1_1)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.1.1\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking Regular String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValstringRegular(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for Regular String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
//testSetPropertyValdouble Test case for String Using ReadOnly and Permanent
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_1_2)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.1.2\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking ReadOnly and Permanent String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValstringReadOnlyPermanent(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for ReadOnly and Permanent String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for String Using Permanent
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_1_3)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.1.3\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking Permanent String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValstringPermanent(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for Permanent String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for String Using Read Only
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_1_4)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.1.4\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking ReadOnly String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValstringReadOnly(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for ReadOnly String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testSetPropertyValdouble Test case for double false false
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_2_1)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.2.1\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking double value with both false param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValdoubleff(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for double Test both false param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testSetPropertyValdouble Test case for double true true
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_2_2)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.2.2\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking double value with both true param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValdoublett(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for double Test both true param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testSetPropertyValdouble Test case for double true false
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_2_3)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.2.3\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking double value with true and false param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValdoubletf(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for double Test true and false param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for double false true
BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_2_4)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.2.4\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking double value with false and true param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValdoubleft(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for double Test false and true param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for int false false
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_3_1)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.3.1\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking int value with both false param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValintff(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for int Test with both false param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}

//testSetPropertyValdouble Test case for int true true
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_3_2)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.3.2\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking int value with both true param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValinttt(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for int Test with both true param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testSetPropertyValdouble Test case for int false true
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_3_3)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.3.3\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking int value with false and true param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValintft(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for int Test with false and true param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
//testSetPropertyValdouble Test case for int true false
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest4_3_4)
	{
		std::cout<<"\nTest Case Number: 4\n";
		std::cout<<"SetPropertyVal Test Case Number: 4.3.4\n";
		std::cout<<"Name: SetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the SetPropertyVal() is taking int value with true and false param Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testSetPropertyValinttf(),true);
		std::cout<<"\n\nOutcome: SetPropertyVal() for int Test with true and false param Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
	
	//testGetPropertyVal Test case for String
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest5_1)
	{
		std::cout<<"\nTest Case Number: 5\n";
		std::cout<<"GetPropertyVal Test Case Number: 5.1\n";
		std::cout<<"Name: GetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the GetPropertyVal() is taking String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testGetPropertyValString(),true);
		std::cout<<"\n\nOutcome: GetPropertyVal() for String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
		
	//testGetPropertyVal Test case for Double
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest5_2)
	{
		std::cout<<"\nTest Case Number: 5\n";
		std::cout<<"GetPropertyVal Test Case Number: 5.2\n";
		std::cout<<"Name: GetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the GetPropertyVal() is taking double value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testGetPropertyValDouble(),true);
		std::cout<<"\n\nOutcome: GetPropertyVal() for Double Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
		
	//testGetPropertyVal Test case for Int
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest5_3)
	{
		std::cout<<"\nTest Case Number: 5\n";
		std::cout<<"GetPropertyVal Test Case Number: 5.3\n";
		std::cout<<"Name: GetPropertyVal Test\n";
		std::cout<<"Objective: Ensure weather the GetPropertyVal() is taking Int value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testGetPropertyValInt(),true);
		std::cout<<"\n\nOutcome: GetPropertyVal() for Int Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
			
	//testDefPropertyValExternal Test case for String
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest6_1)
	{
		std::cout<<"\nTest Case Number: 6\n";
		std::cout<<"testDefPropertyValExternal Test Case Number: 6.1\n";
		std::cout<<"Name: testDefPropertyValExternal Test\n";
		std::cout<<"Objective: Ensure weather the testDefPropertyValExternalString() is taking String value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testDefPropertyValExternalString(),true);
		
			std::cout<<"\n\nOutcome: testDefPropertyValExternalString() for String Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
	//testDefPropertyValExternal Test case for Double
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest6_2)
	{
		std::cout<<"\nTest Case Number: 6\n";
		std::cout<<"testDefPropertyValExternal Test Case Number: 6.2\n";
		std::cout<<"Name: testDefPropertyValExternal Test\n";
		std::cout<<"Objective: Ensure weather the testDefPropertyValExternalDouble() is taking Double value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testDefPropertyValExternalDouble(),true);
		
		std::cout<<"\n\nOutcome: testDefPropertyValExternalDouble() for Double Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}
	
	//testDefPropertyValExternal Test case for Int
	BOOST_AUTO_TEST_CASE(AnonymousJsObjTest6_3)
	{
		std::cout<<"\nTest Case Number: 6\n";
		std::cout<<"testDefPropertyValExternal Test Case Number: 6.3\n";
		std::cout<<"Name: testDefPropertyValExternal Test\n";
		std::cout<<"Objective: Ensure weather the testDefPropertyValExternalInt() is taking Int value Successfully or not\n";
		
		UT_AnonymousJSObject utajo;
		BOOST_CHECK_EQUAL(utajo.testDefPropertyValExternalInt(),true);
		
		std::cout<<"\n\nOutcome: testDefPropertyValExternalInt() for Int Test Successfull\n";
		std::cout<<"---------------------------------------------------------------------------\n";
	}