#ifndef UT_ANONYMOUSCLASS_H
#define UT_ANONYMOUSCLASS_H
#include <boost/test/unit_test.hpp>
#include "js_engine.h"
using boost::unit_test::test_suite;
using boost::shared_ptr;
class UT_AnonymousClass:public test_suite
{
public:
	UT_AnonymousClass();
	 bool testcChkInstance(std::string);
	 bool testGetInstance(std::string);
	 bool testRegister(JSEngine&);
	 bool testGetJSClass(std::string);
	 
};

#endif // UT_ANONYMOUSCLASS_H
