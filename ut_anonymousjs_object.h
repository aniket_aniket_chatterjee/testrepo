#ifndef UT_ANONYMOUSJSOBJECT_H
#define UT_ANONYMOUSJSOBJECT_H
#include "anonymousjsobject.h"
#include "anonymousjsobjectproxy.h"
#include "sproxyFunc_inc.h"
class UT_AnonymousJSObject
{
public:
	UT_AnonymousJSObject();
	~UT_AnonymousJSObject();
	bool testGetJSObject();
	bool testAnonymousJSObject();
	bool testAddFuncJSNative();
	
	bool testAddFunc();
	bool testAddFuncWithParam();
	
	bool testSetPropertyValstringRegular();
	bool testSetPropertyValstringReadOnly();
	bool testSetPropertyValstringPermanent();
	bool testSetPropertyValstringReadOnlyPermanent();
	
	bool testSetPropertyValdoubleff();
	bool testSetPropertyValdoublett();
	bool testSetPropertyValdoubleft();
	bool testSetPropertyValdoubletf();
	
	bool testSetPropertyValintff();
	bool testSetPropertyValinttt();
	bool testSetPropertyValintft();
	bool testSetPropertyValinttf();
	
	bool testGetPropertyValString();
	bool testGetPropertyValDouble();
	bool testGetPropertyValInt();
	
	bool testDefPropertyValExternalString();
	bool testDefPropertyValExternalDouble();
	bool testDefPropertyValExternalInt();
	void genScript(std::string);
};

#endif // UT_ANONYMOUSJSOBJECT_H
